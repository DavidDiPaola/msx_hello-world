#!/bin/sh

# 2022 David DiPaola
# Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

OPENMSX=${OPENMSX:-openmsx}

"$OPENMSX" -machine C-BIOS_MSX1 -cart "$@"
