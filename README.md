# MSX hello world
Simple MSX example programs using GNU binutils.

## Programs

### 00 - Hello World

Prints "HELLO WORLD" to the screen.

![Text printed on a screen](doc/img/00_hello-world.png)

### 01 - Sprites

Displays and moves a sprite around the screen.

![Text printed on a screen](doc/img/01_sprites.png)

## Prerequisites
- GNU binutils for Z80
  - Debian/Ubuntu: `sudo apt install binutils-z80`
- OpenMSX if you want to run or debug the code
  - Debian/Ubuntu: `sudo apt install openmsx openmsx-debugger`
