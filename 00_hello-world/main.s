; 2022 David DiPaola
; Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

.include "msx.inc"

header main

main:
	; init screen
	call INIT32     ; init mode 1
	ld a, 32        ; set width
	ld (LINLEN), a  ; ...

	; print message
	ld hl, message
	call print

	; halt
1:	jr 1b

; print a string to the screen
; parameters:
; 	HL - address of the string
print:
	push af
1:	ld a, (hl)  ; get char
	cp 0        ; if end of string...
	jr z, 1f    ; ...break loop
	call CHPUT  ; print char
	inc hl      ; next char
	jr 1b
1:	pop af
	ret

message:
	.asciz "HELLO WORLD"
