; 2022 David DiPaola
; Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

.include "msx.inc"

header main

main:
	call video_init_screen1

	; print message
	ld d, 12               ; row
	ld e, 12               ; column
	ld hl, message
	call video_text_at
	call video_text_print

	; load sprite 0 tile
	ld a, 0
	ld hl, sprite_tile
	call video_sprite_loadPatt

	; load sprite 0 attributes
	ld a, 0
	ld hl, sprite_attr
	call video_sprite_loadAttr

	; set sprite direction
	ld a, 1
	ld (sprite_dir_x), a
	ld (sprite_dir_y), a

	; main loop
bounce:

	ld a, 0
	call video_sprite_getXY

	; increment X
	ld a, (sprite_dir_x)
	add a, b
	ld b, a

	; bounce off left side?
	cp 1
	jr nc, skip_bounce_xMin
	ld a, 1
	ld (sprite_dir_x), a
skip_bounce_xMin:

	; bounce off right side?
	cp 254
	jr c, skip_bounce_xMax
	ld a, -1
	ld (sprite_dir_x), a
skip_bounce_xMax:

	; increment Y
	ld a, (sprite_dir_y)
	add a, c
	ld c, a

	; bounce off top side?
	cp 1
	jr nc, skip_bounce_yMin
	ld a, 1
	ld (sprite_dir_y), a
skip_bounce_yMin:

	; bounce off right side?
	cp 191
	jr c, skip_bounce_yMax
	ld a, -1
	ld (sprite_dir_y), a
skip_bounce_yMax:
	
	; set X and Y
	ld a, 0
	call video_sprite_setXY

	ld bc, 2048
2:	dec bc
	xor a
	or b
	or c
	jr nz, 2b

	jr bounce

message:
	.asciz "HELLO EGG"

sprite_tile:
	.byte 0b00000000
	.byte 0b00111000
	.byte 0b01111100
	.byte 0b01111100
	.byte 0b11111110
	.byte 0b11111110
	.byte 0b01111100
	.byte 0b00111000

sprite_attr:
	.byte 8              ; Y position
	.byte 64             ; X position
	.byte 0              ; sprite pattern (sprite tile number)
	.byte VDP_COLOR_RED  ; sprite color

.data

sprite_dir_x:
	.byte 0

sprite_dir_y:
	.byte 0
