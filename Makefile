00_SRC = 00_hello-world/main.s
00_OBJ = $(00_SRC:.s=.o)
00_BIN_O = 00_hello-world/cart.o
00_BIN = $(00_BIN_O:.o=.bin)

01_SRC = 01_sprites/main.s common/video.s
01_OBJ = $(01_SRC:.s=.o)
01_BIN_O = 01_sprites/cart.o
01_BIN = $(01_BIN_O:.o=.bin)

TARGET_ASFLAGS = \
	-march=z80 \
	-I common \
	-g

TARGET_LDFLAGS = \
	-T common/linker.lds \
	-O1

TARGET_PREFIX ?= z80-unknown-coff-
TARGET_AS ?= $(TARGET_PREFIX)as
TARGET_LD ?= $(TARGET_PREFIX)ld
TARGET_OBJCOPY ?= $(TARGET_PREFIX)objcopy

.PHONY: all
all: $(00_BIN) $(01_BIN)

.PHONY: clean
clean:
	rm -f $(00_OBJ) $(00_BIN_O) $(00_BIN)
	rm -f $(01_OBJ) $(01_BIN_O) $(01_BIN)

%.o: %.s
	$(TARGET_AS) $(TARGET_ASFLAGS) -o $@ $<

%.bin: %.o
	$(TARGET_OBJCOPY) -S -O binary $< $@

$(00_BIN_O): $(00_OBJ)
	$(TARGET_LD) $(TARGET_LDFLAGS) -o $@ $^

$(01_BIN_O): $(01_OBJ)
	$(TARGET_LD) $(TARGET_LDFLAGS) -o $@ $^
