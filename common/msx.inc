; 2022 David DiPaola
; Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

; ROM calls (see http://map.tni.nl/resources/msxbios.php)
.set RDVRM,  0x004A  ; read byte from VRAM
.set WRTVRM, 0x004D  ; write byte to VRAM
.set LDIRVM, 0x005C  ; copy from memory to VRAM
.set CLRSPR, 0x0069  ; initialize all sprites
.set INIT32, 0x006F  ; set screen mode 1
.set CALPAT, 0x0084  ; get sprite pattern table address
.set CALATR, 0x0087  ; get sprite attribute table address
.set CHPUT,  0x00A2  ; print char
.set CLS,    0x00C3  ; clear screen
.set POSIT,  0x00C6  ; change cursor position

; system variables (see http://map.tni.nl/resources/msxsystemvars.php)
.set LINLEN, 0xF3B0  ; video text line length (1 byte)
.set T32NAM, 0xF3B3  ; video mode 1 name table (2 bytes)
.set T32COL, 0xF3B5  ; video mode 1 color table (2 bytes)
.set T32CGP, 0xF3B7  ; video mode 1 character pattern table (2 bytes)
.set T32ATR, 0xF3B9  ; video mode 1 sprite attribute table (2 bytes)
.set T32PAT, 0xF3BB  ; video mode 1 sprite pattern table (2 bytes)

; IO ports (see http://map.tni.nl/resources/msx_io_ports.php)
.set IO_VDP_DATA, 0x98  ; data read/write
.set IO_VDP_CTRL, 0x99  ; control/status register

; VDP registers (see https://www.smspower.org/uploads/Development/tms9918a.txt)
.set VDP_REG_CTRL0, (1<<7)|0  ; control register 0
.set VDP_REG_CTRL0_EV, 1<<0   ; control register 0 - external video input enable
.set VDP_REG_CTRL0_M2, 1<<1   ; control register 0 - mode bit 2
.set VDP_REG_CTRL1, (1<<7)|1    ; control register 1
.set VDP_REG_CTRL1_MAG,   1<<0  ; control register 1 - sprite magnification - 0: 1x, 1: 2x
.set VDP_REG_CTRL1_SIZE,  1<<1  ; control register 1 - sprite size - 0: 8x8, 1: 16x16
.set VDP_REG_CTRL1_M3,    1<<3  ; control register 1 - mode bit 3 (MSB)
.set VDP_REG_CTRL1_M1,    1<<4  ; control register 1 - mode bit 1 (LSB)
.set VDP_REG_CTRL1_IE,    1<<5  ; control register 1 - vertical blank interrupt enable
.set VDP_REG_CTRL1_BLANK, 1<<6  ; control register 1 - display enable
.set VDP_REG_CTRL1_RAMSZ, 1<<7  ; control register 1 - VRAM size - 0: 4K, 1: 16K
.set VDP_REG_NMTBL, (1<<7)|2  ; name table (background tile map) VRAM address - 0000 xxxx --> xx xx00 0000 0000
.set VDP_REG_CLTBL, (1<<7)|3  ; color table (background tile colors) VRAM address - xxxx xxxx --> xx xxxx xx00 0000
.set VDP_REG_PGAD,  (1<<7)|4  ; pattern generator (background tiles) VRAM address - 0000 0xxx --> xx x000 0000 0000
.set VDP_REG_SATBL, (1<<7)|5  ; sprite addribute table VRAM address - 0xxx xxxx --> xx xxxx x000 0000
.set VDP_REG_SPGAD, (1<<7)|6  ; sprite pattern generator (sprite tiles) VRAM address - 0000 0xxx --> xx x000 0000 0000
.set VDP_REG_TXTCL, (1<<7)|7  ; text and background colors - ffffbbbb, f: foreground, b: background

; VDP colors (see TMS9918 manual)
.set VDP_COLOR_TRANSPARENT,  0x0
.set VDP_COLOR_BLACK,        0x1
.set VDP_COLOR_GREEN,        0x2
.set VDP_COLOR_GREEN_LIGHT,  0x3
.set VDP_COLOR_BLUE_DARK,    0x4
.set VDP_COLOR_BLUE_LIGHT,   0x5
.set VDP_COLOR_RED_DARK,     0x6
.set VDP_COLOR_CYAN,         0x7
.set VDP_COLOR_RED,          0x8
.set VDP_COLOR_RED_LIGHT,    0x9
.set VDP_COLOR_YELLOW_DARK,  0xA
.set VDP_COLOR_YELLOW_LIGHT, 0xB
.set VDP_COLOR_GREEN_DARK,   0xC
.set VDP_COLOR_MAGENTA,      0xD
.set VDP_COLOR_GREY,         0xE
.set VDP_COLOR_WHITE,        0xF


; macros
.macro header entry=0x400A
.ascii "AB"
.word \entry
.byte 0, 0, 0, 0, 0, 0
.endm
