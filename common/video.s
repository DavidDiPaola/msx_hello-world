; 2022 David DiPaola
; Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

.include "msx.inc"

; video modes: https://www.msx.org/wiki/SCREEN

; initialize video to MSX SCREEN 1 mode (9918 mode 0)
; clobbers: none
.global video_init_screen1
video_init_screen1:
	push af
	push bc
	push de

	; set mode to screen 1
	call INIT32

	; set text width
	ld a, 32
	ld (LINLEN), a

	; clear text
	call CLS

	; clear sprites
	call CLRSPR

	pop de
	pop bc
	pop af
	ret

; clear the screen
; clobbers: none
.global video_text_clear
video_text_clear:
	push af
	push bc
	push de

	call CLS

	pop de
	pop bc
	pop af
	ret

; move cursor
; parameters:
; 	D - row
; 	E - column
; clobbers: none
.global video_text_at
video_text_at:
	push af
	push de
	push hl

	ld l, d
	ld h, e
	call POSIT

	pop hl
	pop de
	pop af
	ret

; print a string to the screen
; parameters:
; 	HL - address of the string
; clobbers: HL
.global video_text_print
video_text_print:
	push af

1:	ld a, (hl)  ; get char
	cp 0        ; if end of string...
	jr z, 1f    ; ...break loop
	call CHPUT  ; print char
	inc hl      ; next char
	jr 1b

1:	pop af
	ret

; print a 4bit hex value to the screen
; parameters:
; 	A - value
; clobbers: none
.global video_text_printHex4
video_text_printHex4:
	push af

	and 0xF
	cp 0xA     ; a >= 0xA?
	jr nc, 1f  ; if yes: its 0xA-F

	; numbers 0-9
	add a, 0x30
	jr 2f

	; numbers 0xA-F
1:	sub 0xA
	add a, 0x41

	; print
2:	call CHPUT

	pop af
	ret

; print a 16bit hex value to the screen
; parameters:
; 	A - value
; clobbers: none
.global video_text_printHex8
video_text_printHex8:
	push af
	push bc

	ld b, a

	srl a
	srl a
	srl a
	srl a
	call video_text_printHex4

	ld a, b
	call video_text_printHex4

	pop bc
	pop af
	ret

; print a 16bit hex value to the screen
; parameters:
; 	BC - value
; clobbers: none
.global video_text_printHex16
video_text_printHex16:
	push af
	push bc

	ld a, b
	call video_text_printHex8

	ld a, c
	call video_text_printHex8

	pop bc
	pop af
	ret

; load a single sprite's pattern into VRAM
; parameters:
; 	A - sprite number
; 	HL - address of sprite pattern data
; clobbers: none
.global video_sprite_loadPatt
video_sprite_loadPatt:
	push af
	push bc
	push de
	push hl

	; get sprite pattern VRAM address in DE
	push hl
	call CALPAT
	ex de, hl
	pop hl

	; copy data to VRAM
	ld bc, 8     ; sprite pattern size
	call LDIRVM

	pop hl
	pop de
	pop bc
	pop af
	ret

; load a single sprite's attributes into VRAM
; parameters:
; 	A - sprite number
; 	HL - address of sprite attribute data
; clobbers: none
.global video_sprite_loadAttr
video_sprite_loadAttr:
	push af
	push bc
	push de
	push hl

	; get sprite attribute address in DE
	push hl
	call CALATR
	ex de, hl
	pop hl

	; copy data to VRAM
	ld bc, 4     ; sprite attribute size
	call LDIRVM

	pop hl
	pop de
	pop bc
	pop af
	ret

; get sprite X and Y position
; parameters:
; 	A - sprite number
; returns:
; 	B - X position
; 	C - Y position
; clobbers: none
.global video_sprite_getXY
video_sprite_getXY:
	push af
	push hl

	; get sprite attribute address
	call CALATR

	; get Y coordinate
	call RDVRM
	ld c, a

	; get X coordinate
	inc hl
	call RDVRM
	ld b, a

	pop hl
	pop af
	ret

; set sprite X and Y position
; parameters:
; 	A - sprite number
; 	B - X position
; 	C - Y position
; clobbers: none
.global video_sprite_setXY
video_sprite_setXY:
	push af
	push bc
	push hl

	; get sprite attribute address
	call CALATR

	; set Y coordinate
	ld a, c
	call WRTVRM

	; set X coordinate
	inc hl
	ld a, b
	call WRTVRM

	pop hl
	pop bc
	pop af
	ret
