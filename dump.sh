#!/bin/sh

# 2022 David DiPaola
# Licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

set -eu

ARG_FILE="$1"
shift 1

TARGET_PREFIX=${TARGET_PREFIX:-z80-unknown-coff-}
TARGET_OBJDUMP=${TARGET_OBJDUMP:-${TARGET_PREFIX}objdump}

echo "========== HEADERS =========="
"$TARGET_OBJDUMP" -h "$@" "$ARG_FILE"

echo "========== CODE =========="
"$TARGET_OBJDUMP" -d "$@" "$ARG_FILE"
